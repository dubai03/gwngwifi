package com.Dubai.gwngwifi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class MainActivity extends AppCompatActivity {

    public TextView log = null;
    public EditText usernameinput = null;
    public EditText passwordinput = null;
    public SharedPreferences sharedPreferences = null;

    ;

    public String getoneurlip(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int ipAddress = wifiInfo.getIpAddress();
            return formatIPAddress(ipAddress);
        }
        return null;
    }

    public String formatIPAddress(int ipAddress) {
        return ((ipAddress & 0xFF) + "." +
                ((ipAddress >> 8) & 0xFF) + "." +
                ((ipAddress >> 16) & 0xFF) + "." +
                ((ipAddress >> 24) & 0xFF));
    }


    public static String UA = "Mozilla/5.0 (Linux; Android 7.1.1; MI 6 Build/NMF26X; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.132 MQQBrowser/6.2 TBS/043807 Mobile Safari/537.36 MicroMessenger/6.6.1.1220(0x26060135) NetType/WIFI Language/zh_CN";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        log = findViewById(R.id.校园网view);
        usernameinput = (EditText) findViewById(R.id.username);
        passwordinput = (EditText) findViewById(R.id.password);
        log.setMovementMethod(ScrollingMovementMethod.getInstance());
        // 获取Shared Preferences对象
        sharedPreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        // 读取账号和密码
        String username = sharedPreferences.getString("username", "");
        String password = sharedPreferences.getString("password", "");
        usernameinput.setText(username);
        passwordinput.setText(password);
        log.setText("");
        log.append("使用说明：\n");
        log.append("在开通了校园网的情况下：\n");
        log.append("1.设备请连接wifi：GWNG (此时建议关闭移动数据)\n");
        log.append("2.输入校园网账号和密码，（默认校园网账号是学号，密码是身份证后8位）\n");
        log.append("3.点击校园网上线按钮\n");
        log.append("4.看提示做操作\n");
        log.append("5.【部分手机上，校园网登录成功后助手主动退出，切勿疑惑】\n");

    }


    public Map<String, Object> urlquerytomap(String Ourl) {
        // 创建 URL 对象
        String s1 = Ourl.split("do")[1];
        String str = s1;
        String[] splitURL = str.split("&");
        Map<String, Object> mapParam = new HashMap<String, Object>();
        for (String s : splitURL) {
            if (s.indexOf("=") > 0) {
                mapParam.put(s.substring(0, s.indexOf("=")), s.substring(s.indexOf("=") + 1, s.length()));
            }

        }
        return mapParam;

    }


    public String getoneurlip(boolean 是否认证在校园网内) throws IOException {
//        本函数用于获取本机IP


        //1.通过接口获取ip
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .followRedirects(false)
                .connectTimeout(3, java.util.concurrent.TimeUnit.SECONDS)
                .readTimeout(3, java.util.concurrent.TimeUnit.SECONDS)

                .build();
        Request request = new Request.Builder()
                .url("http://1.1.1.1/")
                .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
                .header("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6")
                .header("Cache-Control", "no-cache")
                .header("Connection", "keep-alive")
                .header("Pragma", "no-cache")
                .header("Upgrade-Insecure-Requests", "1")
                .header("User-Agent", getUA())
                .build();
        Response response = null;

        String resultstr = null;

        if (是否认证在校园网内) {
            OkHttpClient clientq = new OkHttpClient();
            response = clientq.newCall(request).execute();
            resultstr = response.body().string();
            if (resultstr.contains("成功登陆")) {
                log.append("校园网已经登录了！！！" + "\n");
                finish();
                return null;
            }
        } else {
            response = client.newCall(request).execute();
            resultstr = response.headers().get("Location");
//            log.append(resultstr);
            Map<String, Object> urlquerytomap = urlquerytomap(resultstr);
            return (String) urlquerytomap.get("?wlanuserip");
        }


//                    log.append(resultstr+"\n");
        String pattern = "\\\"(https?://[^\\\"]+)\\\"";
        Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        Matcher m = r.matcher(resultstr);
        String Ourl = null;
        if (m.find()) {
            Ourl = m.group(0);
            if (!Ourl.contains("wlanuserip")) {
                log.append("请关闭移动数据只开wifi连接GWNG再重试\n");
                return null;
            }
        } else {
            log.append("NO MATCH" + "\n");//TODO 判断网络环境
            log.append("请关闭移动数据只开wifi连接GWNG再重试\n");
            return null;
        }
        Map<String, Object> urlquerytomap = urlquerytomap(Ourl);
        String ip = (String) urlquerytomap.get("?wlanuserip");
        return ip;


    }

    public String getUA() {
        String userAgent = System.getProperty("http.agent");
        if (userAgent != null && userAgent.length() > 0) {
            return userAgent;
        } else {
            return UA;
        }


    }

//    public  void  下线(View view){
//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                String oneurlgetip = null;
//                try {
//                    oneurlgetip = getoneurlip(false);
//                    if (oneurlgetip==null){
//                        return;
//                    }
//                    log.append(oneurlgetip);
//
//                    OkHttpClient client = new OkHttpClient();
//                    RequestBody formBody = new FormBody.Builder()
//                            .add("wlanacip", "")
//                            .add("wlanuserip", oneurlgetip)
//                            .add("wlanacname", "gwng")
//                            .add("version", "")
//                            .add("portaltype", "")
//                            .build();
//                    Request request = new Request.Builder()
//                            .url("http://10.244.0.13/quickauthdisconn.do")
//                            .post(formBody)
//                            .header("Accept", "application/json, text/javascript, */*; q=0.01")
//                            .header("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6")
//                            .header("Cache-Control", "no-cache")
//                            .header("Connection", "keep-alive")
//                            .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
//                            .header("Origin", "http://10.244.0.13")
//                            .header("Pragma", "no-cache")
//                            .header("User-Agent", getUA())
//                            .header("X-Requested-With", "XMLHttpRequest")
//                            .build();
//
//                    try (Response response = client.newCall(request).execute()) {
//                        if (response.isSuccessful()) {
//                            String json = response.body().string();
//
//                            JSONParser parser = new JSONParser();
//                            JSONObject jsonObject =null;
//                            try {
//                                jsonObject = (JSONObject) parser.parse(json);
//                            } catch (Exception e) {
//                                log.append(e.toString());
//                                return;
//                            }
//                            log.append(jsonObject.toString()+"\n");
//                            if (!Integer.valueOf((String) jsonObject.get("code")).equals(0)){
//                                log.append((String)jsonObject.get("message")+"\n");
//                            }else {
//                                log.append("下线成功！\n");
//                            }
//                        }
//                    }
//                } catch (IOException e1){
//                    log.append(e1.toString()+"\n");
//                    log.append("请关闭移动数据只开wifi连接GWNG再重试");
//                }
//            }
//        });
//        // 启动线程
//        thread.start();
//    }


    public void 绑定wifi() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        Network[] networks = new Network[0];
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            networks = connectivityManager.getAllNetworks();
        }
        for (Network network : networks) {
            NetworkInfo networkInfo = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                networkInfo = connectivityManager.getNetworkInfo(network);
            }
            if (networkInfo.getTypeName().equals("WIFI")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    connectivityManager.bindProcessToNetwork(network);
                    log.append(networkInfo.getTypeName() + "退出 \n ");
                }
                break;
            }
        }
    }




    public void 上线(View view)  {
        Context context=this;
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                // 保存当前的移动数据状态

                log.setText("");
                log.append("执行中..稍作等待..\n");
                SharedPreferences.Editor editor = sharedPreferences.edit();
                // 保存账号和密码
                editor.putString("username", String.valueOf(usernameinput.getText()));
                editor.putString("password", String.valueOf(passwordinput.getText()));
                // 提交更改
                editor.apply();
                绑定wifi();




                try {
//                    String oneurlgetip = getoneurlip(true);
                    String oneurlgetip = getoneurlip(context);
                    
                    log.append(oneurlgetip+" \n");

                    if (oneurlgetip==null || oneurlgetip.length()<=5){
                        log.append("获取本机ip失败\n");

                        return;
                    } else if (oneurlgetip.equals("0.0.0.0")) {
                        log.append("【请wifi连接GWNG】\n");
                        return;
                    } else if (!oneurlgetip.contains("172.")) {
                        log.append("【请检查wifi是否连接GWNG】\n");
                        return;
                    }

                    OkHttpClient client =  new OkHttpClient.Builder()
                            .readTimeout(1000, TimeUnit.MILLISECONDS)//设置读取超时为10秒
                            .connectTimeout(1000, TimeUnit.MILLISECONDS)//设置链接超时为10秒
                            .build();

                    String wlanuserip=oneurlgetip;
                    String userid= String.valueOf(usernameinput.getText());
                    String passwd= String.valueOf(passwordinput.getText());
                    Request loginrequest = new Request.Builder()
                            .url("http://10.244.0.13/quickauth.do?userid="+userid+"&passwd="+passwd+"&wlanuserip="+wlanuserip+"&wlanacname=gwng&portalpageid=1&portaltype=1&hostname=Android")
                            .header("Accept", "application/json, text/javascript, */*; q=0.01")
                            .header("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6")
                            .header("Cache-Control", "no-cache")
                            .header("Connection", "keep-alive")
                            .header("Pragma", "no-cache")
                            .header("User-Agent", getUA())
                            .header("X-Requested-With", "XMLHttpRequest")
                            .build();
                    Response response2 = client.newCall(loginrequest).execute();
                    // 检查响应是否成功
                    if (response2.isSuccessful()) {
                        // 解析JSON响应为Map
                        String json = response2.body().string();
                        JSONParser parser = new JSONParser();
                        JSONObject jsonObject =null;
                        try {
                             jsonObject = (JSONObject) parser.parse(json);
                        } catch (Exception e) {
                            log.append(e.toString());
                            return;
                        }
                        log.append(jsonObject.toString()+"\n");
                        if (!Integer.valueOf((String) jsonObject.get("code")).equals(0)){
                            log.append((String)jsonObject.get("message")+"\n");
                        }else {

                            log.append("【登录成功！】\n");
                            finish();  //关闭程序

                        }
                        // 在这里使用解析后的Map
                    } else {
                        log.append("请求登录失败：" + response2.code() + " - " + response2.message()+"\n");
                    }

                    log.append("执行完毕...."+"\n");
                }
                catch (ConnectException connectException){
                    log.append("请关闭移动数据只开wifi连接GWNG再重试");
                }
                catch (Exception e) {
                    log.append(e.toString()+"\n");
                    log.append("请关闭移动数据只开wifi连接GWNG再重试");
                }
            }
        });
        // 启动线程
        thread.start();


    }
}